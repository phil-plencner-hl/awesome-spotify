const express = require('express');
const router = express.Router();
const qs = require('query-string');
const eta = require('eta');
const spotifyCtrl = require('../controllers/spotify.ctrl');
const discogsCtrl = require('../controllers/discogs.ctrl');
const wikipediaCtrl = require('../controllers/wikipedia.ctrl');

const { body,validationResult } = require('express-validator');

const getUserFromReq = (req) => {
    return req.token && req.token.user || null;
}

router.get("/", async function (req, res) {

    res.render("index", {
        title: "Awesome Spotify!",
        description: "This site contains ideas for making the Spotify experience more awesome.",
    });
});

router.get("/albums/:artistId", async function (req, res) {
    
    let artistId = req.params.artistId;
    let artistImage = '';
    let wikipediaInfo = {};
    
    if (artistId) {

        let page = req.query.page;
        if (!page) {
            page = 1;
        }
        let albums = await spotifyCtrl.getAlbums(artistId, page);
        let totalPages = albums.total / 20;

        if (albums.artist.images.length !== 0) {
            artistImage = albums.artist.images[0].url;
        }
        
        wikipediaInfo = await wikipediaCtrl.getWikiInfo(albums.artist.name);

        res.render("albums", {
            title: albums.artist.name + " Albums!",
            description: "Albums by " + albums.artist.name + " pulled from the Spotify API.",
            artist: albums.artist,
            artistImage: artistImage,
            albums: albums.items,
            total: albums.total,
            page: parseInt(page),
            totalPages: parseInt(totalPages),
            wikipediaExtract: wikipediaInfo.wikipediaExtract,
            wikipediaLink: wikipediaInfo.wikipediaLink,
            wikipediaTitle: wikipediaInfo.wikipediaDisplayTitle,
            wikipediaThumbnail: wikipediaInfo.wikipediaThumbnail,
        });
        
    }

});

router.get("/pandemic/", async function (req, res) {

    playlists = await spotifyCtrl.getPlaylists();

    res.render("pandemic", {
        title: "Pandemic Status Playlists",
        description: "My ongoing series of Spotify playlists helping me survive the pandemic.",
        playlists: playlists.result,
        totalTracks: playlists.totalTracks,
        totalHours: playlists.totalHours,
    });
});

router.get("/search/", async function (req, res) {

    let artistName = req.query.artistName;
    if (!artistName) {
        artistName='';
    }
    artists = await spotifyCtrl.searchArtists(artistName);

    console.log('route artists:');
    console.log(artists);

    res.render("search", {
        title: "Search Artists",
        description: "Search for an artist to listen to.",
        artistName: artistName,
        artists: artists.items,
    });
});

router.get("/discogs/search/", async function (req, res) {

    let artistName = req.query.artistName;
    let modal = req.query.modal;

    if (!artistName) {
        artistName='';
    }

    let albumName = req.query.albumName;
    if (!albumName) {
        albumName='';
    }

    releases = await discogsCtrl.searchReleases(artistName, albumName);

    console.log('route releases:');
    console.log(releases);

    res.render("discogs-search", {
        title: "Discogs Search",
        description: "Search for an albums on Discogs.",
        artistName: artistName,
        albumName: albumName,
        releases: releases.results,
        modal: modal,
    });
});

router.get("/discogs/album/:artistName/:albumName", async function (req, res) {
    
    let artistName = req.params.artistName;
    let albumName = req.params.albumName;
    let modal = req.query.modal;
    let release = {};

    if (!artistName) {
        artistName='';
    }

    if (!albumName) {
        albumName='';
    }

    if (artistName && albumName) {
        releases = await discogsCtrl.searchReleases(artistName, albumName); 
        let validRelease = {};

        if (releases) {
            for(var item of releases.results) {
                if (item.master_id) {
                    validRelease = item;
                    break;    
                }
            }

            releaseId = validRelease.id;
            console.log('releaseId: '+releaseId);
            if (releaseId) {
                release = await discogsCtrl.getRelease(releaseId);
            }
        }


        res.render("discogs", {
            title: "Discogs Test",
            description: "Testing Discogs API.",
            release: release,
            releaseId: releaseId,
            modal: modal,
        });
    }

});    

router.get("/discogs/:releaseId", async function (req, res) {
    
    let releaseId = req.params.releaseId;
    let modal = req.query.modal;

    if (releaseId) {

        let release = await discogsCtrl.getRelease(releaseId);

        res.render("discogs", {
            title: "Discogs Test",
            description: "Testing Discogs API.",
            release: release,
            releaseId: releaseId,
            modal: modal,
        });

    }

});

router.get("/pandemic/player/:playlistId", async function (req, res) {

let playlistId = req.params.playlistId;
let playlistName = '';
let playlistDescription = '';
let playlistImage = '';
let playlist = {};

if (playlistId) {
    let playlist = await spotifyCtrl.getPlaylist(playlistId);
    if (playlist) {
        console.log('playlist name:'+playlist.playlist.name);
        playlistName = playlist.playlist.name;
        playlistDescription = playlist.playlist.description;
        playlistImage = playlist.playlist.images[0].url;
    }
}

res.render("playlist", {
    title: playlistName,
    description: playlistDescription,
    playlistId: playlistId,
    playlist: playlist,
    playlistName: playlistName,
    playlistImage: playlistImage,
});

});

router.get("/player/:albumId", async function (req, res) {

let albumId = req.params.albumId;
let albumName = '';
let artistName = '';
let readableAlbumName = '';
let readableArtistName = '';
let releases = {};
let release = {};
let releaseId = req.query.releaseId;
let wikipediaInfo = {};

if (releaseId) {
    release = await discogsCtrl.getRelease(releaseId);
}

if (albumId) {
    let album = await spotifyCtrl.getAlbum(albumId);
    if (album) {
        readableAlbumName = album.album.name;
        readableArtistName = album.album.artists[0].name;
        artistName = readableArtistName.replace(/[^a-zA-Z0-9 ]/g, "");
        artistName = readableArtistName.replace(/\s+/g, '-').toLowerCase();

        wikipediaInfo = await wikipediaCtrl.getWikiInfo(readableAlbumName);

        albumName = readableAlbumName.replace(/[^a-zA-Z0-9 ]/g, "");
        albumName = readableAlbumName.replace(/\s+/g, '-').toLowerCase();
        if (Object.keys(release).length === 0) {
            releases = await discogsCtrl.searchReleases(artistName, albumName);
        }
    } 
    res.render("player", {
        title: readableAlbumName,
        description: readableArtistName,
        albumId: albumId,
        album: album,
        albumName: albumName,
        artistName: artistName,
        releases: releases.results,
        releaseId: releaseId,
        release: release,
        wikipediaExtract: wikipediaInfo.wikipediaExtract,
        wikipediaLink: wikipediaInfo.wikipediaLink,
        wikipediaTitle: wikipediaInfo.wikipediaDisplayTitle,
        wikipediaThumbnail: wikipediaInfo.wikipediaThumbnail,
    });
}
});

module.exports = router;